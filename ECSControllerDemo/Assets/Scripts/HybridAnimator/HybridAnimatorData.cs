using System;
using Unity.Collections;
using Unity.Entities;

namespace Shiro
{
    public struct UpdateAnimatorFloatParameter : IBufferElementData
    {
        public FixedString32 Parameter;
        public float Value;
    }
}
