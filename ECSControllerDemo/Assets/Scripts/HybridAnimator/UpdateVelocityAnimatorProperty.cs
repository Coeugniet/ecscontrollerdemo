﻿using Unity.Entities;

namespace Shiro
{
    public class UpdateVelocityAnimatorProperty : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref DynamicBuffer<UpdateAnimatorFloatParameter> updateBuffer, in Velocity velocity) =>
            {
                updateBuffer.Add(new UpdateAnimatorFloatParameter() { Parameter = "Velocity", Value = velocity.Magnitude });
            }).WithoutBurst().Run();
        }
    }
}
