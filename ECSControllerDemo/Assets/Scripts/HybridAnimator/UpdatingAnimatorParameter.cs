﻿using Unity.Entities;
using UnityEngine;

namespace Shiro
{
    public class UpdatingAnimatorParameter : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Animator animator, ref DynamicBuffer<UpdateAnimatorFloatParameter> updateBuffer) =>
            {
                for(int i = updateBuffer.Length - 1; i >= 0; i--)
                {
                    animator.SetFloat(updateBuffer.ElementAt(i).Parameter.ToString(), updateBuffer.ElementAt(i).Value);
                    updateBuffer.RemoveAt(i);
                }
            }).WithoutBurst().Run();
        }
    }
}
