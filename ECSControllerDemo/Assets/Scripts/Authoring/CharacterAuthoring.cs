using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Shiro
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(ConvertToEntity))]
    [AddComponentMenu("Authoring/Character")]
    public class CharacterAuthoring : MonoBehaviour
    {
        public GameObject Render;
        public Rigidbody Body;
        public Animator Animator;
        public UnityEngine.InputSystem.PlayerInput Input;
        public Camera Camera;
        public float MovementSpeed;
        public float MaxMovementSpeed;
        [Range(0f, 1f)] public float RotationSpeed;
    }

    public class CharacterConversion : GameObjectConversionSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((CharacterAuthoring authoring) =>
            {
                Entity entity = GetPrimaryEntity(authoring);
                DstEntityManager.SetName(entity, "Character");
                DstEntityManager.AddComponent<PlayerInput>(entity);
                DstEntityManager.AddComponent<Forward>(entity);
                DstEntityManager.AddComponent<Velocity>(entity);
                DstEntityManager.AddComponentData(entity, new MoveForward() { Speed = authoring.MovementSpeed, MaxSpeed = authoring.MaxMovementSpeed });
                DstEntityManager.AddComponentData(entity, new Strafe() { Speed = authoring.MovementSpeed, MaxSpeed = authoring.MaxMovementSpeed });
                DstEntityManager.AddComponentObject(entity, authoring.Body);
                DstEntityManager.AddComponentObject(entity, authoring.Input);
                DstEntityManager.AddComponentObject(entity, authoring.Animator);
                DstEntityManager.AddBuffer<AddForce>(entity);
                DstEntityManager.AddBuffer<UpdateAnimatorFloatParameter>(entity);

                Entity RenderEntity = DstEntityManager.CreateEntity();
                DstEntityManager.SetName(RenderEntity, "CharacterRender");
                DstEntityManager.AddComponent<Forward>(RenderEntity);
                DstEntityManager.AddComponent<SetTransformForward>(RenderEntity);
                DstEntityManager.AddComponentData(RenderEntity, new Parent() { Value = entity });
                if (authoring.RotationSpeed > 0f)
                    DstEntityManager.AddComponentData(RenderEntity, new ForwardCopyVelocity() { LockY = true, Target = entity, LerpSpeed = authoring.RotationSpeed });
                DstEntityManager.AddComponentObject(RenderEntity, authoring.Render.transform);
                

                Entity CameraEntity = DstEntityManager.CreateEntity();
                DstEntityManager.SetName(CameraEntity, "CharacterCamera");
                DstEntityManager.AddComponent<Forward>(CameraEntity);
                DstEntityManager.AddComponentData(CameraEntity, new Parent() { Value = entity });
                DstEntityManager.AddComponentObject(CameraEntity, authoring.Camera.transform);

                DstEntityManager.AddComponentData(entity, new CopyForward() { From = CameraEntity, LockY = true });
            });
        }
    }
}