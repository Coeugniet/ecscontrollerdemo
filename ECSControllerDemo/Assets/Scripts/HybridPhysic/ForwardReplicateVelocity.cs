﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Shiro
{
    public class ForwardReplicateVelocity : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref Forward forward, in ForwardCopyVelocity forwardCopyVelocity) =>
            {
                if (!HasComponent<Velocity>(forwardCopyVelocity.Target)) return;
                var velocity = GetComponent<Velocity>(forwardCopyVelocity.Target);
                if (Magnitude(velocity.Value) < 0.1f) return;
                var forwardGoal = new Vector3(velocity.Value.x, forwardCopyVelocity.LockY ? forward.Value.y : velocity.Value.y, velocity.Value.z).normalized;
                forward.Value = forwardCopyVelocity.LerpSpeed > 0 && forwardCopyVelocity.LerpSpeed < 1 ?
                                Vector3.Lerp(forward.Value, forwardGoal, forwardCopyVelocity.LerpSpeed) :
                                forwardGoal;
            }).WithoutBurst().Run();
        }

        float Magnitude(float3 vector) => math.sqrt(math.pow(vector.x, 2) + math.pow(vector.y, 2) + math.pow(vector.z, 2));
    }
}
