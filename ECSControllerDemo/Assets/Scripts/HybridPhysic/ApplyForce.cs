﻿using Unity.Entities;
using UnityEngine;

namespace Shiro
{
    public class ApplyForce : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity entity, Rigidbody body, ref DynamicBuffer<AddForce> forces) =>
            {
                for (int i = forces.Length - 1; i >= 0; i--)
                {
                    body.AddForce(forces[i].Direction * forces[i].Strength * Time.DeltaTime, forces[i].Mode);
                    forces.RemoveAt(i);
                }
                if (HasComponent<Velocity>(entity)) SetComponent(entity, new Velocity() { Value = body.velocity });
            }).WithoutBurst().Run();
        }
    }
}
