using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Shiro
{
    public struct ForwardCopyVelocity : IComponentData
    {
        public Entity Target;
        public bool LockY;
        public float LerpSpeed;
    }

    public struct AddForce : IBufferElementData
    {
        public float3 Direction;
        public float Strength;
        public ForceMode Mode;
    }

    public struct Velocity : IComponentData
    {
        public float3 Value;

        public float Magnitude => math.length(Value);

        public static explicit operator float3(Velocity velocity) => velocity.Value;
    }
}
