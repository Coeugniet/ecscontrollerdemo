﻿using Unity.Entities;
using UnityEngine;

namespace Shiro
{
    public class Strafing : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref DynamicBuffer<AddForce> forces, in Forward forward, in Strafe strafe, in Velocity velocity) =>
            {
                var direction = -Vector3.Normalize(Vector3.Cross(forward.Value, Vector3.up));
                var strength = strafe.Speed * strafe.Multiplier;
                if (strength == 0f) return;
                if (velocity.Magnitude >= strafe.MaxSpeed) return;
                forces.Add(new AddForce() { Direction = direction, Strength = strength, Mode = ForceMode.Acceleration });
            }).WithoutBurst().Run();
        }
    }
}
