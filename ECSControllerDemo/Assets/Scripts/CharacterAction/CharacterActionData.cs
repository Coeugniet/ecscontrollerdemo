using Unity.Entities;

namespace Shiro
{
    public struct MoveForward : IComponentData
    {
        public float Speed;
        public float MaxSpeed;
        public float Multiplier;
    }

    public struct Strafe : IComponentData
    {
        public float Speed;
        public float MaxSpeed;
        public float Multiplier;
    }
}
