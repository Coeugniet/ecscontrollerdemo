﻿using Unity.Entities;
using UnityEngine;

namespace Shiro
{
    public class MovingForward : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref DynamicBuffer<AddForce> forces, in Forward forward, in MoveForward moveForward, in Velocity velocity) =>
            {
                var direction = Vector3.Normalize(forward.Value);
                var strength = moveForward.Speed * moveForward.Multiplier;
                if (strength == 0f) return;
                if (velocity.Magnitude >= moveForward.MaxSpeed) return;
                forces.Add(new AddForce() { Direction = direction, Strength = strength, Mode = ForceMode.Acceleration });
            }).WithoutBurst().Run();
        }
    }
}
