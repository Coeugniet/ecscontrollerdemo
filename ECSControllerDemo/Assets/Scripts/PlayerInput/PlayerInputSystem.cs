﻿using Unity.Entities;

namespace Shiro
{
    public class PlayerInputSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((UnityEngine.InputSystem.PlayerInput unityInput, ref PlayerInput ecsInput) =>
            {
                ecsInput.Move.x = unityInput.actions["Strafe"].ReadValue<float>();
                ecsInput.Move.y = unityInput.actions["MoveForward"].ReadValue<float>();
                ecsInput.Rotate = unityInput.actions["Rotate"].ReadValue<float>();
            }).WithoutBurst().Run();
        }
    }
}
