﻿using Unity.Entities;

namespace Shiro
{
    public class StrafeWithInput : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref Strafe strafe, in PlayerInput input) =>
            {
                strafe.Multiplier = input.Move.x;
            }).WithoutBurst().Run();
        }
    }
}
