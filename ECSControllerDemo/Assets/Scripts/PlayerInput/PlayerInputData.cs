using Unity.Entities;
using Unity.Mathematics;

namespace Shiro
{
    public struct PlayerInput : IComponentData
    {
        public float2 Move;
        public float Rotate;
    }
}
