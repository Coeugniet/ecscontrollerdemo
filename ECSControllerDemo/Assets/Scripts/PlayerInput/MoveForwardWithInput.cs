﻿using Unity.Entities;

namespace Shiro
{
    public class MoveForwardWithInput : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref MoveForward moveForward, in PlayerInput input) =>
            {
                moveForward.Multiplier = input.Move.y;
            }).WithoutBurst().Run();
        }
    }
}
