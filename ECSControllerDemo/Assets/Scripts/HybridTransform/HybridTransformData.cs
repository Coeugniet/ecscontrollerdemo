using Unity.Entities;
using Unity.Mathematics;

namespace Shiro
{
    public struct CopyForward : IComponentData
    {
        public Entity From;
        public bool LockY;
    }

    public struct Forward : IComponentData
    {
        public float3 Value;
        public static explicit operator float3(Forward forward) => forward.Value;
    }

    public struct SetTransformForward : IComponentData { }
}

