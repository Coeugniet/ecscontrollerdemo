﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Shiro
{
    public class HybridForward : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity entity, Transform transform, ref Forward forward) =>
            {
                if (!HasComponent<SetTransformForward>(entity)) forward.Value = transform.forward;
                else if (!forward.Value.Equals(float3.zero)) transform.forward = forward.Value;
            }).WithoutBurst().Run();
        }
    }
}
