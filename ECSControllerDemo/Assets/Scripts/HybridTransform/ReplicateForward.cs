﻿using Unity.Entities;
using Unity.Mathematics;

namespace Shiro
{
    public class ReplicateForward : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref Forward forward, in CopyForward copy) =>
            {
                if (!HasComponent<Forward>(copy.From)) return;
                var target = GetComponent<Forward>(copy.From);
                forward.Value = new float3
                {
                    x = target.Value.x,
                    y = copy.LockY ? forward.Value.y : target.Value.y,
                    z = target.Value.z
                };
            }).WithoutBurst().Run();
        }
    }
}

